package com.zuitt.example;



public class Main {
    public static void main(String[] args){
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        phonebook.displayContacts();


    }
}
